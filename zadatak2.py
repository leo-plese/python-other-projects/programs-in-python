from sys import argv,exit

def isFloatNum(word):
    try:
        float(word)
    except ValueError:
        return False
    return True

def loadInData(imgInFile):
    inLines = imgInFile.readlines()
    inLinesPrep = []
    for line in inLines:
        lineElems = line.strip().split()
        for el in lineElems:
            if not isFloatNum(el):
                print("Neki element nije realan broj.")
                exit()
        numList = [float(num) for num in lineElems]
        numList.sort()
        inLinesPrep.append(numList)

    #print(inLinesPrep)
    return inLinesPrep

def getHypothesesQuantiles(hypList):
    quantList = []
    for arr in hypList:
        hypLen = len(arr)
        if hypLen == 0:
            continue
        elif hypLen < 10:
            # VAZNO: Ova hipoteza ima manje od 10 brojeva u nizu. Zadnji kvantili bit ce jednaki zadnjem.
            remainLen = 10-hypLen
            lastNum = arr[hypLen-1]
            #print("lastNum", lastNum)
            newList = arr[:]
            newList.extend([lastNum for _ in range(remainLen)])
            #print("newList",newList)
            quantList.append(newList)
        else:
            qInds = [x/10 for x in range(1,10)]
            #print("qInds",qInds)

            quantList.append([arr[round(hypLen*q-1)] for q in qInds])

    #print("quantList",quantList)
    return quantList

def printResult(hypQuantList):
    print("Hyp#Q10#Q20#Q30#Q40#Q50#Q60#Q70#Q80#Q90")
    for i in range(len(hypQuantList)):
        print("%03d" % (i+1), end="")
        hypQs = hypQuantList[i]
        for q in hypQs:
            print("#{}".format(q), end="")
        print("\n", end="")

if __name__=="__main__":
    if len(argv) != 2:
        print("Treba dati ime 1 datoteke.")
        exit()

    datName = argv[1]

    with open(datName) as imgInFile:
        hypList = loadInData(imgInFile)

    #print(hypList)

    hypQuantList = getHypothesesQuantiles(hypList)
    #print(hypQuantList)

    printResult(hypQuantList)

