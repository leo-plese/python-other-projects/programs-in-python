from sys import argv
from os import listdir
import re

def loadData(studentsFile):
    studList = []
    for line in studentsFile:
        studList.append(line.split())

    studDict = {}
    for stud in studList:
        jmbag = stud[0]
        if jmbag in studDict:
            print("Datoteka studenti.txt sadrzava vise puta zapis s istim JMBAG-om.")
            exit()
        studDict[jmbag] = (stud[1], stud[2])

    return studDict

def isFloatNum(word):
    try:
        float(word)
    except ValueError:
        return False
    return True

def loadLabData(labF, labGroupNums, studLabBodDict, studDict):
    #print("labGroupNums",labGroupNums)
    labDataList = []
    for line in labF:
        jmbagBodIn = line.split()
        if not isFloatNum(jmbagBodIn[1]):
            print("Naden zapis s brojem bodova koji nije realan broj.")
            exit()
        labDataList.append((jmbagBodIn[0], float(jmbagBodIn[1])))

        jmbag = jmbagBodIn[0]
        bod = jmbagBodIn[1]
        labNum = int(labGroupNums[0])
        groupNum = labGroupNums[1]

        if labNum in studLabBodDict[jmbag]:
            print("Studentu %s (%s, %s) se po drugi puta pokusava zapisati broj bodova za lab%d (%s bodova u grupi g%s)" %
                  (jmbag, studDict[jmbag][0], studDict[jmbag][1],labNum, bod, groupNum), end=" - ")
            print("vec zapisan za lab%d u grupi g%s s %s bodova" % (labNum, studLabBodDict[jmbag][labNum][1], studLabBodDict[jmbag][labNum][0]))
            return -1
        else:
            studLabBodDict[jmbag][labNum] = (bod, groupNum)

    return labDataList


if __name__=="__main__":
    if len(argv) != 2:
        print("Treba dati ime 1 direktorija s ciljanim datotekama.")
        exit()

    dirName = argv[1]
    try:
        dirFiles = listdir(dirName)
    except:
        print("Ne postoji direktorij",dirName)
        exit()


    labFiles = {}
    for f in dirFiles:
        if f == "studenti.txt":
            continue

        m = re.match("^Lab_([0-9]+)_g([0-9]+).txt$", f)
        if m:
            labFiles[f] = m.groups()

    #print("*****",labFiles)
    allLabNums = set()
    for labFile in labFiles:
        labNum = int(labFiles[labFile][0])
        allLabNums.add(labNum)

    allLabNums = sorted(list(allLabNums))
    #print("*****", allLabNums)

    if "studenti.txt" not in dirFiles:
        print("Nema datoteke studenti.txt u danom direktoriju.")
        exit()

    if len(labFiles) == 0:
        print("Nema datoteka sa zapisima s laboratorija u danom direktoriju.")
        exit()

    with open(dirName+"/studenti.txt", "r", encoding="utf-8") as studentsFile:
        studDict = loadData(studentsFile)


    #print("###",studDict)

    studLabBodDict = {}
    for studJmbag in studDict:
        studLabBodDict[studJmbag] = {}


    for f in labFiles:
        with open(dirName+"/"+f, "r") as labF:
            labDataList = loadLabData(labF, labFiles[f], studLabBodDict, studDict)

            if labDataList == -1:
                print("Prekidam ucitavanje i ispisujem statistiku iz dosad prikupljenih podataka...")
                break


    #print("-----------")
    # VAZNO: studLabBodDict je rjecnik oblika: "{JMBAG: {LAB: (BOD, GRUPA)}}"
    # -> svaki student (JMBAG) ima popis laboratorija (LAB) koji su kljucevi unutrasnjeg rjecnika pridruzenog tom JMBAG-u,
    # dok svakom od tih LAB je priduzen broj bodova koje je student JMBAG ostvario na tom LAB-u unutar grupe GRUPA
    #
    # VAZNA napomena (uvjet zadatka): jedan JMBAG za jedan LAB moze biti zapisan samo jedanput pod jednom grupom (GRUPA),
    # u protivnom funkcija "loadLabData" redom:
    # 1. ispisuje odgovarajuce upozorenje
    # 2. prekida ucitavanje podataka iz trenutnog i svih preostalih jos neprocitanih ciljanih datoteka
    # 3. na kraju se ovdje (od sljedece linije nakon ovog komentara) pa nadalje ispisuju potrebni sazeti podaci *
    # * korak 3. se izvrsava u svakom slucaju, samo je to prijevremeno u slucaju prekrsaja gornjeg uvjeta

    #print(studLabBodDict)
    print("JMBAG      Prezime, Ime        ",end="")
    for l in allLabNums:
        labMark = "L"+str(l)
        print("%-10s" % (labMark), end="")
    print("\n", end="")
    for x in studDict:
        prezimeIme = studDict[x]
        prezimeImeStr = prezimeIme[0] + ", " + prezimeIme[1]
        print("%10s %-20s" % (x, prezimeImeStr), end="")
        #print("JMBAG=",x,studDict[x])
        labBod = studLabBodDict[x]
        #labBodSortedKeys = sorted([x for x in labBod])
        for lab in allLabNums:
            if lab not in labBod:
                bodovi = "0.0"
            else:
                # VAZNO: Ako za studenta ne postoji zapis o br bodova za labos, stavi 0.0 bodova.
                bodovi = labBod[lab][0]

            #print(lab, bodovi)
            print("%-10s" % (bodovi), end="")
        print("\n",end="")








    # with open(datName) as imgInFile:
    #     hypList = loadInData(imgInFile)