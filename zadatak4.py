import urllib.request
import re
from sys import argv, exit

if __name__=="__main__":
    if len(argv) != 2:
        print("Treba dati 1 argument - adresu web stranice.")
        exit()

    webpageName = argv[1]
    # primjeri web str:
    # http://www.python.org
    # https://en.wikipedia.org/wiki/Email_address
    try:
        webpageLoaded = urllib.request.urlopen(webpageName)
    except (urllib.error.URLError, ValueError):
        print("Ne moze otvoriti stranicu", webpageName)
        exit()

    mybytes = webpageLoaded.read()
    mystr = mybytes.decode("utf8")
    print("Web stranica:")
    print("----- POCETAK -----")
    print(mystr)
    print("----- KRAJ -----")
    print()

    print("linkovi:")
    # VAZNO: svaki link se moze ponavljati vise puta ovisno o broju ponavljanju u kodu web stranice!
    linksFound = re.findall('href="(.*?)"', mystr)
    for link in linksFound:
        print(link)
    print()

    # VAZNO: svaki host se ne moze ponavljati (NAPOMENA u tekstu zadatka)!
    hostDict = {}
    hosts = set()
    for i in range(len(linksFound)):
        link=linksFound[i]
        r = re.match("^[(http:)|(https:)]+//(.*)$", link)
        if r:
            linkNoSchema = r.groups()[0]
            firstSlashInd = linkNoSchema.find("/")
            host = linkNoSchema
            if firstSlashInd != -1:
                host = host[:firstSlashInd]
            hosts.add(host)
            if host not in hostDict:
                hostDict[host] = 1
            else:
                hostDict[host] += 1

    print("hostovi: ")
    for h in hosts:
        print(h)
    print()

    print("host : broj referenciranja: ")
    for k in hostDict:
        print(k, ":", hostDict[k])
    print()


    print("email adrese: ")
    mailsFound = re.findall(r'\w+@\w+\.\w+', mystr)
    for mail in mailsFound:
        print(mail)
    print()

    print("slike: ")
    imgsFound = re.findall(r'<img src="(.*?)"(.*?)>', mystr)
    print("UKUPNO slika = ", len(imgsFound))
    # for img in imgsFound:
    #     print(img)