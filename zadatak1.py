from sys import argv, exit

def removeEmptyLinesFromBeginning(inLines):
    for i in range(len(inLines)):
        x=inLines[i].strip()
        if len(x) != 0:
            firstNonEmpty=i
            break

    return inLines[firstNonEmpty:]

def removeEmptyLinesFromEnd(inLines):
    inLinesRev=inLines[::-1]
    res=removeEmptyLinesFromBeginning(inLinesRev)
    res.reverse()

    return res

def containsDecimalPoint(word):
    return "." in word

def isIntNum(word):
    try:
        if containsDecimalPoint(word):
            return False
        int(word)
    except ValueError:
        return False
    return True

def isFloatNum(word):
    try:
        float(word)
    except ValueError:
        return False
    return True



def loadMatrix(matDefLines):
    matDimsRC = matDefLines[0].strip().split()
    dim0In, dim1In = matDimsRC[0], matDimsRC[1]
    #print("matDimsRC",matDimsRC)
    if len(matDimsRC) != 2:
        print("Zapis dimenzija ne sadrzi tocno 2 elementa.")
        exit()
    if not(isIntNum(dim0In) and isIntNum(dim1In)):
        print("Broj redaka ili/i stupaca nije cijeli broj.")
        exit()

    rowNum, colNum = int(dim0In), int(dim1In)

    if rowNum < 1 or colNum < 1:
        print("Broj redaka ili/i stupaca nije prirodan broj.")
        exit()
    #print(rows, cols)

    matDefElemsLines = matDefLines[1:]

    matDict = {}
    if len(matDefElemsLines) == 0:
        return matDict

    for elemLine in matDefElemsLines:
        elemDef = elemLine.split()
        if not(isIntNum(elemDef[0]) and isIntNum(elemDef[1]) and isFloatNum(elemDef[2])):
            print("Indeks retka ili/i stupca matrica nije cijeli broj ili/i vrijednost elementa nije realan broj.")
            exit()

        rowInd, colInd, elemVal = int(elemDef[0]), int(elemDef[1]), float(elemDef[2])
        if rowInd > rowNum or rowInd < 1:
            print("Indeks retka nije u rasponu dozvoljenih indeksa.", rowInd, colInd)
            exit()
        if colInd > colNum or colInd < 1:
            print("Indeks stupca nije u rasponu dozvoljenih indeksa.", rowInd, colInd)
            exit()

        matDict[rowInd, colInd] = elemVal

    return matDict, rowNum, colNum


def stripLines(inLines):
    inLinesStripped = []
    for line in inLines:
        inLinesStripped.append(line.strip())
    return inLinesStripped


def loadMatrices(matInFile):
    inLines = matInFile.readlines()
    inLines=removeEmptyLinesFromBeginning(inLines)
    inLines=removeEmptyLinesFromEnd(inLines)

    if inLines.count("\n") != 1:
        print("Broj praznih redaka izmedu zapisa matrica razlicit od 1.")
        exit()

    emptyLineInd = inLines.index("\n")

    inLines = stripLines(inLines)

    mat1, rowNum1, colNum1 = loadMatrix(inLines[:emptyLineInd])
    mat2, rowNum2, colNum2 = loadMatrix(inLines[emptyLineInd+1:])

    if colNum1 != rowNum2:
        print("Dimenzije matrica nisu uskladene.")
        exit()

    return mat1, mat2, rowNum1, colNum1, rowNum2, colNum2

def mulMats(mat1, mat2):
    newMat = {}
    for rcInds in mat1:
        rInd, cInd = rcInds

        if cInd not in set([x[0] for x in mat2]):
            continue

        newR = rInd
        for x in mat2:
            if x[0] == cInd:
                newC = x[1]
                newVal = mat2[x] * mat1[rInd, cInd]
                newMat[newR,newC] = newVal
                #print(newR, ":", newC, ":", newVal)

    return newMat



def getProdMatDims(rowNum1, colNum1, rowNum2, colNum2):
    rowNum = rowNum1
    colNum = colNum2

    return rowNum, colNum


def writeResult(datName, mat, rowNum, colNum):
    linesOut = []

    keysSorted = sorted(mat.keys(), key=lambda k: (k[0], k[1]))

    linesOut.append("{} {}\n".format(rowNum, colNum))
    for k in keysSorted:
        linesOut.append("{} {} {}\n".format(k[0], k[1], mat[k]))

    linesOut[len(linesOut)-1] = linesOut[len(linesOut)-1].strip()

    datName.writelines(linesOut)

    for line in linesOut:
        print(line.strip())





if __name__=="__main__":
    if len(argv) != 3:
        print("Treba dati ime 2 datoteke.")
        exit()

    dat1Name = argv[1]
    dat2Name = argv[2]
    try:
        with open(dat1Name, "r") as matInFile:
            mat1, mat2, rowNum1, colNum1, rowNum2, colNum2 = loadMatrices(matInFile)
    except FileNotFoundError:
        print("Treba 2 ispravna imena postojecih datoteka.")
        exit()


    newRowNum, newColNum = getProdMatDims(rowNum1, colNum1, rowNum2, colNum2)

    matProd = mulMats(mat1, mat2)
    #print("-------------")
    #print(matProd)

    with open(dat2Name, "w") as matOutFile:
        writeResult(matOutFile, matProd, newRowNum, newColNum)
